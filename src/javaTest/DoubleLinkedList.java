package javaTest;

import junit.framework.TestCase;
import model.data_structures.LinkedList;

public class DoubleLinkedList extends TestCase{
	
	//_______________________________________________
	//Atributos 
	//_______________________________________________
	/**
	 * estructura lista a ser probada
	 * para el test sera una lista de
	 * Strings
	 */
	private LinkedList<String> listaStringTest;

	//_______________________________________________
	//metodos 
	//_______________________________________________

	/**
	 * escenario 1
	 * este escenario representa una 
	 * tipica lista que contiene 
	 * mas de 1 elemento
	 */
	private void setupEscenario1()
	{
		listaStringTest = new LinkedList<String>();
		listaStringTest.add("Daniel");
		listaStringTest.add("Juan");
		listaStringTest.add("Michael");
		listaStringTest.add("Lucy");
	} 
	
	/**
	 * escenario 2
	 * este escenario representa
	 * una lista que 
	 * contiene un unico elemento
	 */
	private void setupEscenario2()
	{
		listaStringTest = new LinkedList<String>();
		listaStringTest.add("Daniel");
	} 

	/**
	 * 
	 */
	public void testAgregarObtener()
	{
		setupEscenario1();
		assertEquals("el String no es el esperado a!=b ", "Daniel", listaStringTest.get(0));
		assertEquals("el String no es el esperado a!=b ", "Juan", listaStringTest.get(1));
		assertEquals("el String no es el esperado a!=b ", "Michael", listaStringTest.get(2));
		assertEquals("el String no es el esperado a!=b ", "Lucy", listaStringTest.get(3));
	}
	
	/**
	 * 
	 */
	public void testAgregarObtener2()
	{
		setupEscenario1();
		assertEquals("el String no es el esperado a!=b ", "Daniel", listaStringTest.get("Daniel"));
		assertEquals("el String no es el esperado a!=b ", "Juan", listaStringTest.get("Juan"));
		assertEquals("el String no es el esperado a!=b ", "Michael", listaStringTest.get("Michael"));
		assertEquals("el String no es el esperado a!=b ", "Lucy", listaStringTest.get("Lucy"));
	}
	
	/**
	 * 
	 */
	public void testEliminarPrimero()
	{
		setupEscenario1();
		listaStringTest.delete("Daniel");
		assertEquals("el String no es el esperado a!=b ", "Juan", listaStringTest.get(0));
		assertEquals("el String no es el esperado a!=b ", "Michael", listaStringTest.get(1));
		assertEquals("el String no es el esperado a!=b ", "Lucy", listaStringTest.get(2));
	}	
	
	/**
	 * 
	 */
	public void testEliminarPrimero2()
	{
		setupEscenario2();
		listaStringTest.delete("Daniel");
		assertNull(listaStringTest.getCurrent());
	}
	
	/**
	 * 
	 */
	public void testTamanio()
	{
		setupEscenario1();
//		assertEquals(4, listaStringTest.size());
//		System.out.println(""+listaStringTest.size());
		listaStringTest.delete("Lucy");
//		listaStringTest.delete("Juan");
//		assertEquals(2, listaStringTest.size());
		System.out.println(""+listaStringTest.get(0));
	}
	
	/**
	 * 
	 */
	public void testIterador()
	{
		setupEscenario2();
		assertEquals("el String no es el esperado a!=b ", "Daniel", listaStringTest.next());
	}

}
