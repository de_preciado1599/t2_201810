package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public class LinkedList <T extends Comparable<T>>  
{
	//_________________________________________________
	//clase auxiliar
	//_________________________________________________
	public class SimpleNode
	{
		//_________________________________________________
		//atributos
		//_________________________________________________
		/**
		 * valor del nodo
		 */
		private T value;
		
		/**
		 * nodo siguiente al actual
		 */
		private SimpleNode siguiente;
		
		/**
		 * nodo anterior al actual
		 */
		private SimpleNode anterior;
		
		//_________________________________________________
		//metodos
		//_________________________________________________
		/**
		 * construyee un nuevo nodo con los valores
		 * recibidos por parametro
		 * @param k key del nuevo nodo
		 * @param v value del nuevo nodo
		 */
		public SimpleNode(T t )
		{
			this.value=t;
			siguiente = null;
			anterior = null;
		}
		
		/**
		 * returna  el siguiente nodo en la
		 * lista
		 * @return null si siguiente == null
		 */
		public SimpleNode darSiguiente()
		{
			return siguiente;
		}
		
		/**
		 * cambia el  nodo siguiente al
		 * actual
		 * @param pSiguiente 
		 */
		public void cambiarSiguiente(SimpleNode pSiguiente)
		{
			siguiente = pSiguiente;
		}
		
		/**
		 * returna  el anterior nodo en la
		 * lista
		 * @return null si anterior == null
		 */
		public  SimpleNode darAnterior()
		{
			return anterior;
		}
		/**
		 * cambia el  nodo anterior al
		 * actual por pAnterior
		 * @param pAnterior
		 */
		public void cambiarAnterior(SimpleNode pAnterior)
		{
			anterior = pAnterior;
		}
		
		/**
		 * returna  el valor guardado en 
		 * el nodo de la lista
		 * @return null si value == null
		 */
		public  T darValor()
		{
			return value;
		}
		/**
		 * cambia el  valor value al
		 * recibido por pValue
		 * @param pValue
		 */
		public void cambiarAnterior(T pValue)
		{
			value = pValue;
		}
		
	
		
}
		
	
	//_________________________________________________
	//atributos
	//_________________________________________________
	/**
	 * primer nodo de la lista
	 */
	private SimpleNode cabeza;
	
	/**
	 * nodo actual de la lista
	 */
	private SimpleNode actual;
	
	/**
	 * cant de elementos en la lista
	 */
	private int tamanio;
	//_________________________________________________
	//metodos
	//_________________________________________________
	
	public LinkedList()
	{
		tamanio = 0;
		cabeza = null;
		actual = null;
		
	}
	
	/**
	 * 
	 * @param pAagregar
	 * @return
	 */
	public void add(T pAagregar)
	{
		cabeza = add(cabeza,pAagregar);
		
	} 
	/**
	 * metodo auxiliar para agregar un nodo a la 
	 * lista
	 * @param pNodo
	 * @param pAagregar
	 * @return
	 */
	public SimpleNode add(SimpleNode pNodo, T pAagregar)
	{
		if(pNodo == null) 
		{
			tamanio++;
			return new SimpleNode(pAagregar);
		}
		else 
		{
			pNodo.siguiente =  add(pNodo.siguiente,pAagregar);
		}
		return pNodo;

	}

	/**
	 * 
	 * @param pAeliminar
	 * @return
	 */
	public boolean delete(T pAeliminar)
	{
		boolean respuesta = delete(cabeza,pAeliminar);
		return respuesta;		
	} 
	
	public Boolean delete (SimpleNode pNodo,T pAeliminar)
	{
		Boolean retorno = null;
		SimpleNode anterior ;
		SimpleNode siguiente;
		if(pNodo.darSiguiente() != null)
		{
			siguiente = pNodo.darSiguiente();
		}
		if(pNodo.darAnterior() != null)
		{
			anterior = pNodo.darAnterior();
		}
		
		
		if(cabeza.darValor().compareTo(pAeliminar) == 0)
		{
			cabeza = pNodo.siguiente;
			tamanio--;
			retorno = true;
			return retorno;
		}
		else if(pNodo.darValor().compareTo(pAeliminar)!=0)
		{
			if( pNodo.siguiente != null)
			{
				delete( pNodo.siguiente, pAeliminar);
			}
			
		}
		else if(pNodo.darValor().compareTo(pAeliminar)==0)
		{
			 pNodo.anterior.cambiarSiguiente( pNodo.siguiente);
			 pNodo.siguiente.cambiarAnterior( pNodo.anterior);
			tamanio --;
			retorno = true;
		}
		return retorno;
	}
	
	/**
	 * 
	 * @return
	 */
	public T get(T aBuscar)
	{
		T respuesta = get(cabeza,aBuscar);
		return respuesta;
	} 
	
	/**
	 * 
	 * @param pNodo
	 * @param aBuscar
	 * @return
	 */
	public T get(SimpleNode pNodo,T aBuscar)
	{
		T retorno = null; 
		
		if(cabeza.darValor().compareTo(aBuscar)==0)
		{
			retorno = cabeza.darValor();
			return retorno;
		}
		else if(pNodo.darValor().compareTo(aBuscar)==0)
		{
			retorno = pNodo.darValor();
			return retorno;
		}
		else
		{
			retorno = get(pNodo.siguiente, aBuscar);
		}
		return retorno;
	}
	
	/**
	 * 
	 * @return
	 */
	public T get(int pPosicion)
	{
		T respuesta = get(cabeza,pPosicion,0);
		return respuesta;
	} 
	
	/**
	 * 
	 * @param nodo
	 * @param pPosicion
	 * @return
	 */
	public T get(SimpleNode pNodo, int pPosicion,int pContador)
	{
		T retorno = null;
		int contador = pContador;
		if(pPosicion == 0)
		{
			retorno = cabeza.darValor();
			return retorno;
		}
		else if(contador == pPosicion)
		{
			retorno = pNodo.darValor();		
		}
		else
		{
			contador ++;
			retorno =  get(pNodo.darSiguiente(),pPosicion, contador);
		}
		
		return retorno;
		
	}
	
	/**
	 * 
	 * @return
	 */
	public int size()
	{
		return tamanio;
	}
	
	/**
	 * 
	 * @return
	 */
	public T getCurrent ()
	{
		if(cabeza!= null)
		{
			return actual.darValor();
		}
		else
		{
			return null;
		}
		

	} 

	/**
	 * 
	 * @return
	 */
	public T next ()
	{
		T retorno = null;
		if(actual== null)
		{
			actual= cabeza;
		}
		else 
			actual = actual.darSiguiente();
			retorno = actual.darValor(); 
			return retorno;
	} 
	
	/**
	 * 
	 * @return
	 */
	public boolean hasNext()
	{
		if(actual.darSiguiente()!=null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}
