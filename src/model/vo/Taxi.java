package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	//___________________________________________________
	//Atributos
	//___________________________________________________
	
	/**
	 * id del taxi
	 */
	private String taxi_id;

	/**
	 * nombre de la compa�ia
	 */
	private String company;
	
	//___________________________________________________
	//Metodos
	//___________________________________________________
	
	/**
	 * metodo constructor
	 * @param pTaxiId
	 * @param pCompany
	 */
	public Taxi(String pTaxiId, String pCompany)
	{
		taxi_id = pTaxiId;
		company = pCompany;
	}

	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() 
	{
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() 
	{
		// TODO Auto-generated method stub
		return company;
	}
	
	/**
	 * 
	 */
	public int compareTo(Taxi pTaxi) 
	{
		// TODO Auto-generated method stub
		int retorno = taxi_id.compareTo(pTaxi.getTaxiId());
		return retorno;
	}	
}
