package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	
	//___________________________________________________
	//Atributos
	//___________________________________________________
	
	/**
	 * id del atxiq ue hizo el servicio
	 */
	private String taxi_id;
	
	/**
	 * id del viaje
	 */
	private String trip_id;

	/**
	 * viaje en segundos
	 */
	private int trip_seconds;

	/**
	 * viaje en millas
	 */
	private double trip_miles;
	
	/**
	 * viaje total
	 */
	private double trip_total;

	/**
	 * zona comunal de fin de un servicio
	 */
	private int dropoff_community_area;

	//___________________________________________________
	//Metodos
	//___________________________________________________
	
	/**
	 * 
	 * @param pTaxiId
	 * @param pTripId
	 * @param pTripSeconds
	 * @param pTripMiles
	 * @param pTripTotal
	 */
	public Service (String pTaxiId, String pTripId, int pTripSeconds, double pTripMiles, double pTripTotal, int pZona)
	{
		taxi_id= pTaxiId;
		trip_id=pTripId;
		trip_seconds = pTripSeconds;
		trip_miles = pTripMiles;
		trip_total= pTripTotal;
		dropoff_community_area = pZona;
	}
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		// TODO Auto-generated method stub
		return taxi_id;
	}
		
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		// TODO Auto-generated method stub
		return trip_id;
	}	
	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		// TODO Auto-generated method stub
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		// TODO Auto-generated method stub
		return trip_total;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getDropoffCommunityArea() 
	{
		// TODO Auto-generated method stub
		return dropoff_community_area;
	}
//	/**
//	 * 
//	 * @return
//	 */
//	public String darDropoffCommunityArea() 
//	{
//		// TODO Auto-generated method stub
//		return dropoff_community_area;
//	}

	/**
	 * @param pServicio
	 */
	public int compareTo(Service pServicio) 
	{
		// TODO Auto-generated method stub
		int retorno = trip_id.compareTo(pServicio.getTripId());
		return retorno;
	}
}
