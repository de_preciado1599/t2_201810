package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.*;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;

public class TaxiTripsManager implements ITaxiTripsManager {
	
	//___________________________________________________
	//Atributos
	//___________________________________________________
	
	/**
	 * crea una lisa doblemente enlazada
	 * que contiene los servicios
	 */
	private LinkedList<Service> servicios;

	/**
	 * crea una lista doblemente enlazada
	 * que contiene taxis
	 */
	private LinkedList<Taxi> taxis;
	
	//___________________________________________________
	//metodos
	//___________________________________________________

	// TODO
	// Definition of data model 
	
	/**
	 * 
	 * @param serviceFile 
	 */
	public void loadServices (String serviceFile) 
	{
		JsonParser parser = new JsonParser();
		servicios = new LinkedList<Service>();
		taxis = new LinkedList<Taxi>();

		try
		{
			
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

			// Tratar cada JsonObject del JsonArray 
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);


				// Obtener la propiedad company desde un servicio ( String ) 
				String company= "NaN";
				if(obj.get("company") != null)
				{
					company = obj.get("company").getAsString();
				}

				// Obtener la propiedad taxi_id desde un servicio ( String )
				String taxi_id= "NaN";
				if(obj.get("taxi_id") != null)
				{
					taxi_id = obj.get("taxi_id").getAsString();
				}

				// Obtener la propiedad trip_id desde un servicio ( String ) 
				String trip_id = "NaN";
				if(obj.get("trip_id").getAsString() != null)
				{
					trip_id = obj.get("trip_id").getAsString();
				}

				// Obtener la propiedad tripSeconds desde un servicio ( int ) 
				int trip_seconds = 0;
				if(obj.get("trip_seconds").getAsInt() != 0)
				{
					trip_seconds = obj.get("trip_seconds").getAsInt();
				}

				// Obtener la propiedad tripMiles desde un servicio ( double ) 
				Double trip_miles = 0.0;
				if(obj.get("trip_miles").getAsDouble() != 0)
				{
					trip_miles = obj.get("trip_miles").getAsDouble();
				}

				// Obtener la propiedad tripTotal desde un servicio ( double ) 
				Double trip_total = 0.0;
				if(obj.get("trip_total").getAsDouble() != 0)
				{
					trip_miles = obj.get("trip_total").getAsDouble();
				}
//				int dropoff_community_area = 0;
//				if(obj.get("dropoff_community_area").getAsInt() != 0)
//				{
//					dropoff_community_area = obj.get("dropoff_community_area").getAsInt();
//				}
//				
//				Service servicio = new Service(taxi_id, trip_id, trip_seconds, trip_miles, trip_total,dropoff_community_area);
				Taxi taxi = new Taxi(taxi_id, company);
//				servicios.add(servicio);
				taxis.add(taxi);
			}
		}
		catch (JsonIOException excepcion1 ) {
			// TODO Auto-generated catch block
			excepcion1.printStackTrace();
		}
		catch (JsonSyntaxException excepcion2) {
			// TODO Auto-generated catch block
			excepcion2.printStackTrace();
		}
		catch (FileNotFoundException excepcion3) {
			// TODO Auto-generated catch block
			excepcion3.printStackTrace();
		}

		System.out.println("Inside loadServices with " + serviceFile);

	}

	/**
	 * 
	 * @param
	 */
	public LinkedList<Taxi> getTaxisOfCompany(String pCompany) 
	{
		// TODO Auto-generated method stub
		LinkedList<Taxi> taxisSameCompany = new LinkedList<Taxi>();
		Taxi actual = taxis.next();
		while(taxis.hasNext()!=false)
		{
			if(actual.getCompany().equals(pCompany))
			{
				Taxi temporal= new Taxi(actual.getTaxiId(), actual.getCompany());
				taxisSameCompany.add(temporal);
			}
			actual = taxis.next();
		}
			System.out.println("Inside getTaxisOfCompany with " + pCompany);
			return taxisSameCompany;
		

	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int pCommunityArea) 
	{
		// TODO Auto-generated method stub
		LinkedList<Service> servicesSameArea = new LinkedList<Service>();
		Service actual = servicios.next();
		while(servicios.hasNext()!=false)
		{
			if(actual.getDropoffCommunityArea()== pCommunityArea)
			{
				Service temporal= new Service(actual.getTaxiId(),actual.getTripId(),actual.getTripSeconds(),actual.getTripMiles(),actual.getTripTotal(),actual.getDropoffCommunityArea());
				servicesSameArea.add(temporal);
			}
			actual = servicios.next();
		}
		System.out.println("Inside getTaxiServicesToCommunityArea with " + pCommunityArea);
		return null;
	}


}
